const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
const web3 = new Web3(ganache.provider());


const compileddonationCamp = require('../build/donationCamp.json');

let accounts;
let factory;
let donationCampAddress;
let donationCamp;

beforeEach(async () => {
  accounts = await web3.eth.getAccounts();

  factory = await new web3.eth.Contract(JSON.parse(compiledFactory.interface))
    .deploy({ data: compiledFactory.bytecode })
    .send({ from: accounts[0], gas: '1000000' });

  
  [donationCampAddress] = await factory.methods.getDeployeddonationCamps().call();
  donationCamp = await new web3.eth.Contract(
    JSON.parse(compileddonationCamp.interface),
    donationCampAddress
  );
});

describe('donationCamps', () => {
  
  it('marks caller as the Dapp manager', async () => {
    const manager = await donationCamp.methods.manager().call();
    assert.equal(accounts[0], manager);
  });

  it('allows people to  amountDonate donation ', async () => {
    await donationCamp.methods. amountDonate().send({
      value: '200',
      from: accounts[1]
    });
    const isContributor = await donationCamp.methods.approvers(accounts[1]).call();
    assert(isContributor);
  });

  it('requires value of product', async () => {
    try {
      await donationCamp.methods. amountDonate().send({
        value: 'value',
        from: accounts[1]
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
  });

  it('allows a manager to make a payment request', async () => {
    await donationCamp.methods
      .donorList('1202','kba', '10', true)
      .send({
        from: accounts[0],
        gas: '1000000'
      });
    const request = await donationCamp.methods.requests(0).call();

    assert.equal('1202', request.description);
  });

  it('processes requests', async () => {
    await donationCamp.methods. amountDonate().send({
      from: accounts[0],
      value: web3.utils.toWei('10', 'ether')
    });

    await donationCamp.methods
      .addDonation('A', web3.utils.toWei('5', 'ether'), accounts[1])
      .send({ from: accounts[0], gas: '1000000' });

    await donationCamp.methods.selectItem(0).send({
      from: accounts[0],
      gas: '1000000'
    });

    await donationCamp.methods.transfer(0).send({
      from: accounts[0],
      gas: '1000000'
    });

    let balance = await web3.eth.getBalance(accounts[1]);
    balance = web3.utils.fromWei(balance, 'ether');
    balance = parseFloat(balance);

    
  });
});
