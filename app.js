

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');



var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var setRequest = require("./routes/setRequest");
var getRequest = require("./routes/getRequest");
var contribute = require("./routes/contribute");
var transfer = require("./routes/transfer");


//-------------------WEB3 Integration starts-----------------------

var path            = require('path');
var MyContractJSON  = require(path.join(__dirname, 'build/contracts/DonationCamp.json'));

var Web3 = require("web3");

const web3 = new Web3('http://localhost:9545');


accountAddress = "0x47573725Ec4a2543Cc95563A29Af7f6d696ccC3e"; 
const contractAddress = MyContractJSON.networks['5777'].address;
const contractAbi = MyContractJSON.abi;

MyContract = new web3.eth.Contract(contractAbi, contractAddress);

//-------------------WEB3 Integration Stops------------------------

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));



app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/setRequest', setRequest);
app.use('/getRequest', getRequest);
app.use('/contribute', contribute);
app.use('/transfer', contribute);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
