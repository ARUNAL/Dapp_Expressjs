pragma solidity^0.5.0;


contract donationCamp{
  
    struct voter{
        string Name;
        uint  netWorth;
        string  constituency;
      }
    mapping(address => bool) funder;
    uint public totalCount;
    
    mapping (uint => voter)public candidate;
    uint public Amount;
    
    uint public maximumDonation;
    address public manager;
    
     modifier electionCommission() {
        require(msg.sender == manager);
        _;
    }
    constructor() public {
        manager = msg.sender;
       }
    

    //to set the value for candidate 
    
    function setRequest(uint _nominationId, string memory _name,  uint _netWorth, string memory _constituency) public {
        
        candidate[_nominationId] = voter(_name,_netWorth,_constituency);
         }

    //to get the value for candidate 

   function searchRequest(uint _nominationId) public view returns (string memory _name,  uint _netWorth, string memory _constituency){
        _name = candidate[_nominationId].Name;
        _netWorth = candidate[_nominationId].netWorth;
        _constituency = candidate[_nominationId].constituency;
          }
          //the person who donate campgain will became 
          function contribute(uint _nominationId ) public payable returns(uint) {
              require(msg.value <= Amount);
              funder[msg.sender]= true;
              totalCount++;
                  
    }
    //Transmite the colllective money after check
    function transfer(uint _nominationId) public  electionCommission returns(bool){
        msg.sender.transfer(Amount);
        return true;
        
    }

}