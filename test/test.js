const json = artifacts.require("DonationCamp");
const interface = json['abi'];
const bytecode = json['bytecode'];

let accounts;
let donCamp;
let Owner;
let ownerBalance;

beforeEach(async () => {
  accounts = await web3.eth.getAccounts();  
  Owner = accounts[0];                     
  donCamp = await new web3.eth.Contract(interface)   
    .deploy({ data: bytecode })
    .send({ from: Owner, gas: '4000000' });
 });
 contract('DonationCamp', () => {
  it('deploys a contract', async () => {
    const donCampAddress = await donCamp.options.address; 
    assert.ok(donCampAddress, 'Test fail');
 
  });
 
  });
 //Checking the functionality-- Add Candidate Details
 it('Candidate Details Added Successfully', async () => {
  candId = 1101;
  Name = "Candidate1";
  PancardNo = "Cand01";
  constituency = "tvm1";
  balance = 0;
try {
    await donCamp.methods.addCandidate(candId, Name, PancardNo, constituency, balance).send({ from: accounts[1], gas: '4000000' });
    candidateDetail = await donCamp.methods.candidateList(candId).call();
    console.log(candidateDetail);
    assert.equal(candidateDetail[0], candId, "Test Fail");
    assert.equal(candidateDetail[1], Name, "Test Fail");
    assert.equal(candidateDetail[2], PancardNo, "Test Fail");
    assert.equal(candidateDetail[3], constituency, "Test Fail");
    assert.equal(candidateDetail[4], balance, "Test Fail");
  }
  catch (err) {
    assert(err);
    console.log(err);
  }
});

//Checking the functionality-- Donor Registration
it('Donor Registration Done Successfuly', async() => {
  voterId = 101;
  Name = "abcd";
  amount = 0;
  regstd = 0;
  try{
    await donCamp.methods.donorRegn(voterId, Name).send({from:accounts[1], gas:'4000000'});
    donorDetail = await donCamp.methods.donorList().call();
    console.log(donorDetail);
    assert.equal(donorDetail[0], voterId, "Test Fail");
    assert.equal(donorDetail[1], Name, "Test Fail");
    assert.equal(donorDetail[2], amount, "Test Fail");
    assert.equal(donorDetail[3], regstd, "Test Fail");
  }
  catch (err) {
    assert(err);
    console.log(err);
  }
});

//Checking the functionality-- Ether/Wei Donation
it('Ether/Wei Donation', async () => {
  donorAddress = accounts[1];
  donCampAddress = await donCamp.options.address;
  contractBalance = await web3.eth.getBalance(donCampAddress);
  console.log(contractBalance);
  candId = 1101;
  amount = 3;

  try {
    await donCamp.methods.amountDonate(candId).send({ from: accounts[1], value: web3.utils.toWei(amount.toString(), 'wei'), gas: '4000000' });
    CurrentContractBalance = await web3.eth.getBalance(donCampAddress);
    console.log(CurrentContractBalance);
    Difference = CurrentContractBalance - contractBalance;
    console.log(Difference);
    assert.equal(Difference, amount, "Test Fail");
  }
  catch (err) {
    assert(err);
    console.log(err);}
});